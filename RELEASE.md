
As on date 17 Novemeber 2013 following files are submitted 
001_remove_extra_hypen.py
002_remove_unique_elements.py
003_median.py
004_most_wanted_letter.py
005_Speech_Module.py
006_tic_tac_toe.py
007_roman_numerals.py
008_morse_clock.py
009_open_labyrinth.py
010_simple_list.py
011_prime_palindrome.py
012_incomplete_triangle.py
013_hamming_distance.py
014_simple_equation.py
015_atm.py
016_purchase_negotiator.py
017_strange_circuit.py
019_bullet_ray_wall_lineseg.py
020_simplify_poli.py
021_readable_numbers.py
022_russian_lucky_ticket.py
023_greatest_common_divisor.py
024_repeating_decimals.py
025_largest_sum_pyramid.py
026_decrypter.py
027_jump_through.py
028_simple_addition_cypher.py
029_broken_clock.py
030_striped_word.py
031_disposable_teleport.py
032_valid_ips.py
033_number_factory.py
034_brackets.py
036_house_password.py

Following are not yet complete
018_hex_spiral.py
035_find_sequence.py
