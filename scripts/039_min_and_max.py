def helper(myop, *args, **kwargs):
    myargs = args
    if len(args) == 1:
        # Exception if the only arg is not iterable
        _=(_e for _e in args[0])
        myargs = args[0]

    key = kwargs.get("key", lambda x: x)
    myargs = list(myargs)
    result = myargs[0]
    for arg in myargs[1:]:
        if myop(key(result), key(arg)): result = arg
    return result

def min(*args, **kwargs):
    myop = lambda x,y: x > y
    return helper(myop, *args, **kwargs)


def max(*args, **kwargs):
    myop = lambda x,y: x < y
    return helper(myop, *args, **kwargs)


if __name__ == '__main__':
    #These "asserts" using only for self-checking and not necessary for auto-testing
    assert max(3, 2) == 3, "Simple case max"
    assert min(3, 2) == 2, "Simple case min"
    assert max([1, 2, 0, 3, 4]) == 4, "From a list"
    assert min("hello") == "e", "From string"
    assert max(2.2, 5.6, 5.9, key=int) == 5.6, "Two maximal items"
    assert min([[1, 2], [3, 4], [9, 0]], key=lambda x: x[1]) == [9, 0], "lambda key"
