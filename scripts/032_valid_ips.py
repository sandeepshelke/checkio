"""
You are given some text that can contain different IP-addresses. You 
should find all correct IPv4 addresses written in dot-decimal notation; 
(Read about IP-addresses here). We don't know about the subnet mask, so
addresses ending with 0 and 255 are considered to be correct for our purposes.
All "words" in the text must be separated by whitespaces.

Your function should return a list of strings in the order of how these IPs
appears in the text.

Input: A block text. A string (unicode).

Output: IPs. A list of strings.
"""
import re

def is_valid(ip):
    #Invalid characters
    if re.compile('[^0-9,^.]').search(ip):
        return False
        
    parts = ip.split('.')
    #Invalid length
    if not (15 >= len(ip) >= 7) or len(parts) != 4:
        return False
    for part in parts:
        if not (255 >= int(part) >= 0):
            return False
    return True

def checkio(text):
    """
        find all IPs
    """
    ips = []
    for ip in text.split():
        if is_valid(ip):
            ips += [ip]
    return ips

#These "asserts" using only for self-checking and not necessary for auto-testing
if __name__ == '__main__':
    assert checkio("192.168.1.1 and 10.0.0.1 or 127.0.0.1") == \
        ["192.168.1.1", "10.0.0.1", "127.0.0.1"], "All correct"
    assert checkio("10.0.0.1.1 but 127.0.0.256 1.1.1.1") == \
        ["1.1.1.1"], "Only 1.1.1.1"
    assert checkio("167.11.0.0 1.2.3.255 192,168,1,1") == \
        ["167.11.0.0", "1.2.3.255"], "0 and 255"
    assert checkio("267.11.0.0 1.2.3.4/16 192:168:1:1") == \
        [], "I don't see IP"
    assert checkio("00250.00001.0000002.1 251.1.2.1") == \
        ["251.1.2.1"], "Be careful with zeros"
