DIRNAMES = 'NWES'
N,W,E,S = DIRNAMES
DIRS = {S:4, N:-4, E:1, W:-1}

def make_graph(data, bhut):
    blocked = (W, N, N, N+E, W, '', '', E, W, '', '', E, W+S, S, S, E+S)
    OPP = {S:N, N:S, E:W, W:E}
    affected = {DIRS[d]+bhut:OPP[d] for d in DIRS}
    graph = {}
    for i,b in enumerate(zip(data, blocked), 1):
        nogo = affected.get(i, '')
        b = b[0]+b[1]
        possible = set(DIRNAMES)^set(b)
        graph[i] = [(d,DIRS[d]+i) for d in possible if d if d != nogo]
    return graph

def bfs(graph, start, end=1, noway='No way'):
    path, cells = '', [start]
    que = [(path, cells)]
    while que:
        # edges and path
        path, cells = que.pop(0)
        if cells[-1] == end: return path
        last_node = cells[-1] # last visited node
        for d,c in graph[last_node]: # adjascent nodes to last visited node
            if c in cells: continue # ignore the edge if used in present path
            np, nc = path + d, cells + [c] # next path and corresponding edges
            que.append((np, nc))
    return noway

def checkio(house, zampya, bhut):
    if zampya == 1: return N
    graph = make_graph(house, bhut)
    noway = ''.join([p[0] for p in graph[zampya]])
    path = bfs(graph, zampya, end=1, noway=noway)
    return path[0]

if __name__ == '__main__':
    #This part is using only for self-checking and not necessary for auto-testing
    from random import choice
    def check_solution(func, house):
        stephan = 16
        ghost = 1
        for step in range(30):
            direction = func(house[:], stephan, ghost)
            if direction in house[stephan - 1]:
                print('Stefan ran into a closed door. It was hurt.')
                return False
            if stephan == 1 and direction == "N":
                print('Stefan has escaped.')
                return True
            stephan += DIRS[direction]
            if ((direction == "W" and stephan % 4 == 0) or (direction == "E" and stephan % 4 == 1) or
                    (stephan < 1) or (stephan > 16)):
                print('Stefan has gone out into the darkness.')
                return False
            sx, sy = (stephan - 1) % 4, (stephan - 1) // 4
            ghost_dirs = [ch for ch in "NWES" if ch not in house[ghost - 1]]
            if ghost % 4 == 1 and "W" in ghost_dirs:
                ghost_dirs.remove("W")
            if not ghost % 4 and "E" in ghost_dirs:
                ghost_dirs.remove("E")
            if ghost <= 4 and "N" in ghost_dirs:
                ghost_dirs.remove("N")
            if ghost > 12 and "S" in ghost_dirs:
                ghost_dirs.remove("S")

            ghost_dir, ghost_dist = "", 1000
            for d in ghost_dirs:
                new_ghost = ghost + DIRS[d]
                gx, gy = (new_ghost - 1) % 4, (new_ghost - 1) // 4
                dist = (gx - sx) ** 2 + (gy - sy) ** 2
                if ghost_dist > dist:
                    ghost_dir, ghost_dist = d, dist
                elif ghost_dist == dist:
                    ghost_dir += d
            ghost_move = choice(ghost_dir)
            ghost += DIRS[ghost_move]
            if ghost == stephan:
                print('The ghost caught Stephan.')
                return False
        print("Too many moves.")
        return False

    assert check_solution(checkio,
                          ["", "S", "S", "",
                           "E", "NW", "NS", "",
                           "E", "WS", "NS", "",
                           "", "N", "N", ""]), "1st example"
    assert check_solution(checkio,
                          ["", "", "", "",
                           "E", "ESW", "ESW", "W",
                           "E", "ENW", "ENW", "W",
                           "", "", "", ""]), "2nd example"
