def is_family(tree):
    famitly_tree, fathers, allsons = {}, [], []
    for f,s in tree:
        famitly_tree.setdefault(f, [])
        famitly_tree[f].append(s)
        if f not in fathers: fathers.append(f)
        if s not in allsons: allsons.append(s)

    for tf in list(fathers):
        tsons = famitly_tree[tf]
        # only one can be fatherless ancestor
        if tf not in allsons: continue
        # you can not be own father
        if tf in tsons: continue
        for f, sons in famitly_tree.items():
            # you cannot be father's father
            if f in famitly_tree[tf] and tf in sons: continue
            # you cannot be father of your brother
            if [s for s in sons if s in tsons]: continue
            if tf in fathers: fathers.remove(tf)
    # only one can be fatherless ancestor
    return len(set(fathers)) == 1


if __name__ == "__main__":
    #These "asserts" using only for self-checking and not necessary for auto-testing
    assert is_family([
      ['Logan', 'William'],
      ['Logan', 'Jack'],
      ['Mike', 'Mike']
    ]) == False, 'Can not be own father'
    assert is_family([
        ["Logan","William"],
        ["William","Jack"],
        ["Jack","Mike"],
        ["Mike","Alexander"]
        ]) == True, 'Long family'
    assert is_family([
      ['Logan', 'Mike']
    ]) == True, 'One father, one son'
    assert is_family([
      ['Logan', 'Mike'],
      ['Logan', 'Jack']
    ]) == True, 'Two sons'
    assert is_family([
      ['Logan', 'Mike'],
      ['Logan', 'Jack'],
      ['Mike', 'Alexander']
    ]) == True, 'Grandfather'
    assert is_family([
      ['Logan', 'Mike'],
      ['Logan', 'Jack'],
      ['Mike', 'Logan']
    ]) == False, 'Can you be a father for your father?'
    assert is_family([
      ['Logan', 'Mike'],
      ['Logan', 'Jack'],
      ['Mike', 'Jack']
    ]) == False, 'Can you be a father for your brather?'
    assert is_family([
      ['Logan', 'William'],
      ['Logan', 'Jack'],
      ['Mike', 'Alexander']
    ]) == False, 'Looks like Mike is stranger in Logan\'s family'
    print("Looks like you know everything. It is time for 'Check'!")
