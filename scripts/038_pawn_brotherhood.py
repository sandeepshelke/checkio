def safe_pawns(pawns):
    safe = 0
    for c,r in pawns:
        cp = chr(ord(c) - 1)
        cn = chr(ord(c) + 1)
        rp = str(int(r) - 1)
        if cp+rp in pawns or cn+rp in pawns:
            safe += 1
    return safe

if __name__ == '__main__':
    #These "asserts" using only for self-checking and not necessary for auto-testing
    assert safe_pawns({"b4", "d4", "f4", "c3", "e3", "g5", "d2"}) == 6
    assert safe_pawns({"b4", "c4", "d4", "e4", "f4", "g4", "e5"}) == 1