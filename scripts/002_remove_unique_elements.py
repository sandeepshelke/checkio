def checkio(data):
    nul = [] #non-unique-list
    if len(data) == 0: return nul
    if data[0] in data[1:]: nul.append(data[0])
    for i in range(1, len(data)):
        el = data[i]
        if el in data[0:i]+data[i+1:]: nul.append(el)
    return nul
    
checkio([1,3,2,1,3])
checkio([1,2,3,4,5])
checkio([5,5,5,5,5])
