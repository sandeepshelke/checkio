
def checkio(line):
    n = line.replace('--', '-')
    while n.count('--') > 0:
        n = n.replace('--','-')
    return n

#These "asserts" using only for self-checking and not necessary for auto-testing    
if __name__ == '__main__':
    assert checkio('I---like--python') == "I-like-python", 'Example'
