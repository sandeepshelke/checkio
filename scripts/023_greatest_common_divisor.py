"""To write the rest of the song, Sofia needs to come up with a perfect rhythm
according to the Euclidean Algorithm. In order to calculate it she will need
the greatest common divisor of two positive numbers (n>0). Write a function
that will calculate a greatest common divisor of two numbers.

Input: A list of two integers.

Output: The greatest common divisor. An integer.

Example:
checkio((12, 8)) == 4
checkio((14, 21)) == 7
checkio((13, 11)) == 1
How it is used: GCD is a basis for the mathematics software.
"""

def gcd(a, b):
    while b != 0:
        t = b
        b = a%b
        a = t
    return a

def checkio(data):
    a, b = data
    if a < b:
        a = a+b
        b = a-b
        a = a-b
    #replace this for solution
    return gcd(a,b)


#These "asserts" using only for self-checking and not necessary for auto-testing
if __name__ == '__main__':
    assert checkio((12, 8)) == 4, "First example"
    assert checkio((14, 21)) == 7, "Second example"
    assert checkio((13, 11)) == 1, "Third example"
