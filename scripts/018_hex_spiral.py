"""Hexagon
"""

def getCenter(base):
    b = base-1
    i, j = 0, 0
    if b%2 == 0:
        i = int(b/2)
        j = i
    else:
        j = int(b/2)
        i = b - j
    return [i,j]

def getEmptyGrid(base):
    grid = []
    for _ in range(base):
        grid += [[0]*base]
    return grid

def generateGrid(num):
    base = int(num**0.5)+1
    highest = base*base
    assert highest >= num, 'wrong base'
    grid = getEmptyGrid(base)
    count = 1
    i, j = getCenter(base)
    grid[i][j] = count
    ur = 1
    dl = 2
    while True:
        for _ in range(ur):
            if count == highest:
                break
            i -= 1
            count += 1
            grid[i][j] = count

        for _ in range(ur):
            if count == highest:
                break
            j += 1
            count += 1
            grid[i][j] = count

        for _ in range(dl):
            if count == highest:
                break
            i +=1
            count += 1
            grid[i][j] = count

        for _ in range(dl):
            if count == highest:
                break
            j -= 1
            count += 1
            grid[i][j] = count

        if count == highest:
            break
        ur += 2
        dl += 2
        
    return grid, base

def getPos(grid, base, val):
    found = False
    for i in range(base):
        for j in range(base):
            if grid[i][j] == val:
                found = True
                break
        if found: break

    return [i,j]

def checkio(data):
    grid, base = generateGrid(max(data))
    a, b = data
    first = getPos(grid, base, a)
    second = getPos(grid, base, b)
    
    i,j = first
    k,m = second
    print(first, a)
    print(second, b)
    dist = abs(i-k) + abs(j-m)

    #replace this for solution
    return dist

print(checkio([2,9]))
print(checkio([6,19]))
print(checkio([5,11]))

#These "asserts" using only for self-checking and not necessary for auto-testing
# if __name__ == '__main__':
#     assert checkio([1, 9]) == 2, "First"
#     assert checkio([9, 1]) == 2, "Reverse First"
#     assert checkio([10, 25]) == 1, "Neighbours"
#     assert checkio([5, 9]) == 4, "Diagonal"
#     assert checkio([26, 31]) == 5, "One row"
#     assert checkio([50, 16]) == 10, "One more test"