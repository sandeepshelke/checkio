E, W, S, N = 'E','W','S','N'

def make_graph(data):
    key = lambda i,j: str(i)+str(j)
    graph = {}
    for i in range(1, 11):
        for j in range(1, 11):
            if data[i][j]: continue
            edges = []
            if not data[i][j+1]: edges.append((key(i, j+1),E))
            if not data[i][j-1]: edges.append((key(i, j-1),W))
            if not data[i+1][j]: edges.append((key(i+1, j),S))
            if not data[i-1][j]: edges.append((key(i-1, j),N))
            if edges: graph[key(i,j)] = edges
    return graph

def bfs(graph, start='11'):
    path, cells = '', [start]
    que = [(path, cells)]
    end = set(('10','10'))
    while que:
        # edges and path
        path, cells = que.pop(0)
        if cells[-1] == '1010': return path
        last_node = cells[-1] # last visited node
        for c,d in graph[last_node]: # adjascent nodes to last visited node
            if c in cells: continue # ignore the edge if used in present path
            np, nc = path + d, cells + [c] # next path and corresponding edges
            que.append((np, nc))
    return 'No way!'

def get_path(data):
    se, es = True, True
    for i in range(1,11):
        se = se and not data[1][i] and not data[i][10]
        es = es and not data[i][1] and not data[10][i]
    if se: return 'SSSSSSSSSEEEEEEEEE'
    if es: return 'EEEEEEEEESSSSSSSSS'
    graph = make_graph(data)
    return bfs(graph)

def checkio(data):
    return get_path(data)


#This code using only for self-checking and not necessary for auto-testing
if __name__ == '__main__':
    #be careful with infinity loop
    print(checkio([
        [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
        [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
        [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
        [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
        [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
        [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
        [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
        [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
        [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
        [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
        [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
        [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
    ]))
    print(checkio([
        [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
        [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
        [1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1],
        [1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1],
        [1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1],
        [1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1],
        [1, 0, 0, 0, 1, 1, 0, 1, 1, 1, 0, 1],
        [1, 0, 1, 0, 0, 0, 0, 1, 0, 1, 1, 1],
        [1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 1],
        [1, 0, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1],
        [1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1],
        [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]]))
    print(checkio([
        [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
        [1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1],
        [1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 1],
        [1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1],
        [1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 1],
        [1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1],
        [1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 1],
        [1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1],
        [1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 1],
        [1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1],
        [1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1],
        [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
        ]))
    print(checkio([
        [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
        [1, 0, 0, 0, 1, 1, 1, 0, 1, 0, 0, 1],
        [1, 0, 1, 0, 0, 0, 1, 0, 1, 1, 0, 1],
        [1, 0, 1, 1, 1, 0, 0, 0, 1, 0, 0, 1],
        [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
        [1, 0, 1, 1, 0, 1, 1, 0, 0, 1, 0, 1],
        [1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1],
        [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1],
        [1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1],
        [1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1],
        [1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1],
        [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
    ]))
    print(checkio([[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1], 
               [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1], 
               [1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1], 
               [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1], 
               [1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1], 
               [1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1], 
               [1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1], 
               [1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1], 
               [1, 1, 1, 1, 1, 0, 1, 0, 0, 1, 0, 1], 
               [1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1], 
               [1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1], 
               [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]]))
