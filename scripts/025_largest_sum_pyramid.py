"""
Consider a list of lists in which the first list has one integer and each
consecutive list has one more integer then the last. Such a list of lists
would look like a data. You must write a program that will help Nikola
and Stephen look for a route down the pyramid by stepping down and to the
left or to the right. Your goal is to make sure this program will find the
sturdiest route, that is, the route with the highest possible sum on its way
down the pyramid.

Tip: Think of each step down to the left as moving to the same index location
or to the right as one index location higher.

sum-in-triangles

Input: A list of lists. Each list contains integers.

Output: An Integer.

Example:

checkio([
    [1],
    [2, 3],
    [3, 3, 1],
    [3, 1, 5, 4],
    [3, 1, 3, 1, 3],
    [2, 2, 2, 2, 2, 2],
    [5, 6, 4, 5, 6, 4, 3]
]) == 23
checkio([
    [1],
    [2, 1],
    [1, 2, 1],
    [1, 2, 1, 1],
    [1, 2, 1, 1, 1],
    [1, 2, 1, 1, 1, 1],
    [1, 2, 1, 1, 1, 1, 9]
]) == 15
assert checkio([
    [9],
    [2, 2],
    [3, 3, 3],
    [4, 4, 4, 4]
]) == 18
How it is used: This is a classical problem, which show to you how to use
dynamical programming.

"""

def checkio(data):
    """list[list[int]] -> int
    Return max possible sum in a path from top to bottom
    """
    rows = len(data)
    maxSum = data[rows - 1]
    
    for i in range(rows - 2, -1, -1):
        for j in range(i+1):
            maxSum[j] = data[i][j] + max(maxSum[j], maxSum[j + 1])

    #replace this for solution
    return max(maxSum)

#These "asserts" using only for self-checking and not necessary for auto-testing
if __name__ == '__main__':
    assert checkio([
        [1],
        [2, 3],
        [3, 3, 1],
        [3, 1, 5, 4],
        [3, 1, 3, 1, 3],
        [2, 2, 2, 2, 2, 2],
        [5, 6, 4, 5, 6, 4, 3]
    ]) == 23, "First example"
    assert checkio([
        [1],
        [2, 1],
        [1, 2, 1],
        [1, 2, 1, 1],
        [1, 2, 1, 1, 1],
        [1, 2, 1, 1, 1, 1],
        [1, 2, 1, 1, 1, 1, 9]
    ]) == 15, "Second example"
    assert checkio([
        [9],
        [2, 2],
        [3, 3, 3],
        [4, 4, 4, 4]
    ]) == 18, "Third example"
