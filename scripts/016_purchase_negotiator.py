"""Write a program that will (depending on Sofia and the old man’s initial
bargaining sums--the steps by which they will increase or decrease the price
during their negotiations) calculate which final price they will agree upon.
If Sofia's offer is lower than or equal to the old man's offer, she will
accept the old man's price and vice versa.

Sofia makes her offer first. She never offers an amount higher than what is
offered to her. On the other hand, the old man never offers an amount lower
than what is offered to him.

Input data: A list, that contains four integer numbers: Sofia's initial offer,
Sofia's raise to her offer, the initial counteroffer from the old man, and the
old man's reduction in his offer;
Output data: The amount of money that Sofia will pay for the spaceship. An integer.
Example:
    checkio([150, 50, 1000, 100]) == 450
    #Sofia: 200
    #Oldman: 900
    #Sofia: 250
    #Oldman: 800
    #Sofia: 300
    #Oldman: 700
    #Sofia: 350
    #Oldman: 600
    #Sofia: 400
    #Oldman: 500
    #Sofia: 450
    #... old man will be ok with it, because his next proposition will be lower than 450.
 
    # a bit shorter example
    checkio([500, 300, 700, 100]) == 700
    #Sofia will be ok with 700 because her next proposition will be higher
"""

def checkio(data):
    b_init, b_raise, s_init, s_red = data
    
    #If saler's initial offer is less than buyers's initial or next
    #offer then accept saler's initial offer
    if b_init >= s_init or b_init+b_raise >= s_init:
        return s_init
    
    b_next, s_next = b_init+b_raise, s_init
    deal = 0
    while True:
        if b_next >= s_next:
            return s_next
        s_next -= s_red
        if s_next <= b_next:
            return b_next
        b_next += b_raise

    return deal

#These "asserts" using only for self-checking and not necessary for auto-testing
if __name__ == '__main__':
    assert checkio([150, 50, 1000, 100]) == 450, "1st example"
    assert checkio([150, 50, 900, 100]) == 400, "2nd example"
