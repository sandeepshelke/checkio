EAST, WEST, SOUTH, NORTH = 'E','W','S','N',

visit = [[0,0]]

def visited(d):
    global visit
    if d in visit:
        return True
    return False

def getFirstMove(data, i, j, start_dir):
    global visit
    path = start_dir

    if data[i+1][j] == 0:
        i+=1
        path = SOUTH
    elif data[i][j+1] == 0:
        j+=1
        path = EAST
    elif data[i-1][j] == 0:
        i -= 1
        path = NORTH
    elif data[i][j-1] == 0:
        j -= 1
        path = WEST

    visit += [[i,j]]
    return i, j, path

def getNextMove(data, i, j, path):
    global visit
    curr = path[-1]
    if curr == NORTH:
        if data[i][j+1] == 0 and not visited([i, j+1]):
            j+=1
            path+=EAST
        elif data[i-1][j] == 0 and not visited([i-1, j]):
            i-=1
            path+=NORTH
        elif data[i][j-1] == 0 and not visited([i, j-1]):
            j-=1
            path+=WEST
        elif data[i+1][j] == 0:
            #If previous move was towards South and now need to step back then 
            #mark the current cell as blocked and remove previous step from path
            path = path[:-1] 
            data[i][j] = 2
            i+=1
    
    elif curr == SOUTH:
        if data[i+1][j] == 0 and not visited([i+1, j]):
            i+=1
            path+=SOUTH
        elif data[i][j+1] == 0 and not visited([i, j+1]):
            j+=1
            path+=EAST
        elif data[i][j-1] == 0 and not visited([i, j-1]):
            j-=1
            path+=WEST
        elif data[i-1][j] == 0:
            #If previous move was towards North and now need to step back then 
            #mark the current cell as blocked and remove previous step from path
            path = path[:-1] 
            data[i][j] = 2
            i-=1
        
    elif curr == WEST:
        if data[i+1][j] == 0 and not visited([i+1, j]):
            i+=1
            path+=SOUTH
        elif data[i][j-1] == 0 and not visited([i, j-1]):
            j-=1
            path+=WEST
        elif data[i-1][j] == 0 and not visited([i-1, j]):
            i-=1
            path+=NORTH
        elif data[i][j+1] == 0:
            #If previous move was towards WEST and now need to step back then 
            #mark the current cell as blocked and remove previous step from path
            path = path[:-1] 
            data[i][j] = 2
            j+=1
        
    elif curr == EAST:
        if data[i][j+1] == 0 and not visited([i, j+1]):
            j+=1
            path+=EAST
        elif data[i+1][j] == 0 and not visited([i+1, j]):
            i+=1
            path+=SOUTH
        elif data[i-1][j] == 0 and not visited([i-1, j]):
            i-=1
            path+=NORTH
        elif data[i][j-1] == 0:
            #If previous move was towards EAST and now need to step back then 
            #mark the current cell as blocked and remove previous step from path
            path = path[:-1] 
            data[i][j] = 2
            j-=1

    visit += [[i,j]]
    return data, i, j, path

def buildMatrix(data):
    """Generates new matrix from input matrix
    e.g. [[1,2,3],[4,5,6],[7,8,9]] ==> 
    [[-1,-1,-1,-1,-1],[-1,1,2,3,-1],[-1,4,5,6,-1],[-1,7,8,9,-1],[-1,-1,-1,-1,-1]]
    """
    N = len(data)
    d = []
    for i in range(N+2):
        d += [[-1]*(N+2)]

    for i in range(N):
        for j in range(N):
            d[i+1][j+1] = data[i][j]
            
    return d

def checkio(data):
    nd, start, end = data
    i, j = start
    x2, y2 = end
    nd = buildMatrix(nd)
    
    global visit
    visit = [start]
    i,j,path = getFirstMove(nd, i, j, SOUTH)
    prev_move = [i,j]
    while True:
        if path == '':
            i,j,path = getFirstMove(nd, i, j, EAST)
        nd, i, j, path = getNextMove(nd, i, j, path)
        if [i, j] == prev_move:
            return False
        else:
            prev_move = [i,j]
        #print path
        if i == x2 and j == y2:
            break #Reached target
    
    return True

checkio([[
    [0,0,8,0,0,3,5],
    [0,8,0,0,9,0,5],
    [8,0,1,2,0,0,3],
    [0,7,0,0,0,0,0],
    [9,0,1,0,0,8,0],
    [9,0,4,0,9,0,0],
    [3,0,6,0,0,0,0]],
    [2,1],[1,2]])

if __name__ == '__main__':
    assert checkio(([
        [0, 0, 5, 4, 0],
        [0, 1, 5, 0, 0],
        [0, 0, 0, 7, 2],
        [8, 0, 0, 0, 0],
        [0, 9, 0, 1, 0]],
        [1, 1], [5, 5])) == True, 'First example'

    assert checkio([[
        [0, 0, 5, 4, 0],
        [0, 0, 5, 0, 0],
        [0, 0, 0, 7, 2],
        [8, 0, 0, 4, 0],
        [0, 9, 0, 1, 0]],
        [1,1], [1,5]]) == False, 'Second example'