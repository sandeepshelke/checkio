units = ["zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"]
teens = ["", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"]
tens = ["", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"]

#FIRST_TEN = ["zero", "one", "two", "three", "four", "five", "six", "seven",
#             "eight", "nine"]
#SECOND_TEN = ["ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen",
#              "sixteen", "seventeen", "eighteen", "nineteen"]
#OTHER_TENS = ["twenty", "thirty", "forty", "fifty", "sixty", "seventy",
#              "eighty", "ninety"]
#HUNDRED = "hundred"


def checkio(num):
    if num == 1000:
        return 'one thousand'
    h = int(num/100)
    tu = num%100
    word = ''
    if h > 0:
        word = units[h] + ' hundred'
        if tu <= 0:
            return word

    if tu <= 19:
        if len(word) > 0:
            word += ' '
        if tu == 10:
            word += tens[int(tu/10)]
        elif tu < 10:
            word += units[tu]
        else:
            word += teens[tu%10]
        return word

    t = int(tu/10)
    u = tu%10
    if len(word) > 0:
        word += ' '
    word += tens[t]
    if u > 0:
        word += ' ' + units[u]
    return word

#Some hints
#Don't forget strip whitespaces at the end of string


#These "asserts" using only for self-checking and not necessary for auto-testing
if __name__ == '__main__':
    assert checkio(4) == 'four', "1st example"
    assert checkio(133) == 'one hundred thirty three', "2nd example"
    assert checkio(12) == 'twelve', "3rd example"
    assert checkio(101) == 'one hundred one', "4th example"
    assert checkio(212) == 'two hundred twelve', "5th example"
    assert checkio(40) == 'forty', "6th example"
