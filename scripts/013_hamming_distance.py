"""Hamming distance between two numbers
Example:
117 = 0 1 1 1 0 1 0 1
 17 = 0 0 0 1 0 0 0 1
  H = 0+1+1+0+0+1+0+0 = 3
Here H is hamming distance
"""

def checkio(data):
    a,b = data
    if a == b: return 0

    ba = bin(a).replace('0b', '')
    bb = bin(b).replace('0b', '')
    la = len(ba)
    lb = len(bb)
    if la != lb:
        prepad = abs(la - lb)
        if la > lb:
            bb = '0'*prepad + bb
        else:
            ba = '0'*prepad + ba

    hamming = ''
    for i in range(len(ba)):
        if ba[i] != bb[i]:
            hamming += '1'
        else:
            hamming += '0'
    return sum([int(i) for i in hamming])

def checkio_new(data):
    a,b = data
    if a == b: return 0

    ba = bin(a).replace('0b', '')
    bb = bin(b).replace('0b', '')
    la = len(ba)
    lb = len(bb)
    prepad = abs(la - lb)
    if la > lb:
        bb = '0'*prepad + bb
    elif la < lb:
        ba = '0'*prepad + ba

    hd = 0
    for i in range(len(ba)):
        hd += 1 if ba[i] != bb[i] else 0

    return hd
