"""
Input:A list of 3 strings. First string--start time. Second string--end
(incorrect) time. Time is given in the format "hh:mm:ss" (Examples: "01:16:59"
and "23:00:13"). Third string is a description of the clock error in the
format "+(-)N [second, minute, hour](s) at M [second, minute, hour](s)"
(Example: "+1 second at 10 seconds"--the clock is 1 second fast for every
10 seconds of the real time period and "-5 minutes at 5 hours"--the clock lags
5 minutes for every 5 hours of actual time."

Output:The real time. String in the format "hh:mm:ss." The result is rounded
down to the nearest second (Example: "00:00:01.9" = "00:00:01").

Example:
checkio(['00:00:00', '00:00:15', '+5 seconds at 10 seconds']) ==  '00:00:10'
checkio(['06:10:00', '06:10:15', '-5 seconds at 10 seconds']) ==  '06:10:30'
checkio(['13:00:00', '14:01:00', '+1 second at 1 minute']) ==  '14:00:00'
checkio(['01:05:05', '04:05:05', '-1 hour at 2 hours']) ==  '07:05:05'
checkio(['00:00:00', '00:00:30', '+2 seconds at 6 seconds']) ==  '00:00:22'
How it is used: Here you can learn how to work with time (for example datetime)
"""
from datetime import datetime
intervals = {'second':1, 'seconds':1, 'minute':60, 'minutes':60, 'hour':3600, 'hours':3600} 

def getTimeErrPerSec(desc):
    d = desc.split()
    global intervals
    diff = int(d[0])*intervals[d[1]]
    interval = int(d[3])*intervals[d[4]]
    err = diff/interval
#     if diff > 0:
#         err = interval/(interval+diff)
    return err

def checkio(data):
    start, end, description = data
    err = getTimeErrPerSec(description)
    mask = '%H:%M:%S'
    start = datetime.strptime(start, mask)
    end = datetime.strptime(end, mask)
    tdiff = end - start
    n = end - tdiff*err/(1+err)
    return n.strftime(mask)

#These "asserts" using only for self-checking and not necessary for auto-testing
if __name__ == "__main__":
    assert checkio(['00:00:00', '00:00:15', '+5 seconds at 10 seconds']) == '00:00:10', "First example"
    assert checkio(['06:10:00', '06:10:15', '-5 seconds at 10 seconds']) == '06:10:30', 'Second example'
    assert checkio(['13:00:00', '14:01:00', '+1 second at 1 minute']) == '14:00:00', 'Third example'
    assert checkio(['01:05:05', '04:05:05', '-1 hour at 2 hours']) == '07:05:05', 'Fourth example'
    assert checkio(['00:00:00', '00:00:30', '+2 seconds at 6 seconds']) == '00:00:22', 'Fifth example'
    assert checkio(["11:02:00","20:30:10","-5 seconds at 1 hour"]) == "20:30:57", 'sixth'