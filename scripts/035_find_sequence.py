"""
You are given a matrix of NxN (4<=N<=10). You should check if there is a
sequence of 4 or more matching numbers. The sequence may be positioned
horizontally, vertically or diagonally.

Input: A list of lists. Each list contains integers.
Output: A boolean. Wether or not a sequence exists.
"""

def check_vertical(mat, N):
    for j in range(N):
        for i in range(N-3):
            count = 1
            for k in range(i+1, N):
                if mat[i][j] != mat[k][j]:
                    break
                else:
                    count += 1
            if count >= 4:
                return True
    return False

def check_horizontal(mat, N):
    for i in range(N):
        for j in range(N-3):
            count = 1
            for k in range(j+1, N):
                if mat[i][j] != mat[i][k]:
                    break
                else:
                    count += 1
            if count >= 4:
                return True
    return False

def check_diagonal(mat, N):
    return False

def check_inverse_diagonal(mat, N):
    return False

def checkio(matrix):
    N = len(matrix)
    if check_vertical(matrix, N):
        return True
    if check_horizontal(matrix, N):
        return True
    if check_diagonal(matrix, N):
        return True
    if check_inverse_diagonal(matrix, N):
        return True
    #replace this for solution
    return False

#These "asserts" using only for self-checking and not necessary for auto-testing
if __name__ == '__main__':
    assert checkio([
        [1, 2, 1, 1],
        [1, 1, 4, 1],
        [1, 3, 1, 6],
        [1, 7, 2, 5]
    ]) == True, "Vertical"
    assert checkio([
        [7, 1, 4, 1],
        [1, 2, 5, 2],
        [3, 4, 1, 3],
        [1, 1, 8, 1]
    ]) == False, "Nothing here"
    assert checkio([
        [2, 1, 1, 6, 1],
        [1, 3, 2, 1, 1],
        [4, 1, 1, 3, 1],
        [5, 5, 5, 5, 5],
        [1, 1, 3, 1, 1]
    ]) == True, "Long Horizontal"
    assert checkio([
        [7, 1, 1, 8, 1, 1],
        [1, 1, 7, 3, 1, 5],
        [2, 3, 1, 2, 5, 1],
        [1, 1, 1, 5, 1, 4],
        [4, 6, 5, 1, 3, 1],
        [1, 1, 9, 1, 2, 1]
    ]) == True, "Diagonal"
