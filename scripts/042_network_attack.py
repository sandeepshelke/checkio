def make_graph(matrix):
    graph = {i: {} for i in range(len(matrix))}
    for i in range(len(matrix)):
        for j in range(len(matrix)):
            if i==j: continue
            if matrix[i][j] == 0 or matrix[j][i] == 0: continue
            weight = matrix[j][j]
            if weight == 0: continue
            graph[i][j] = weight

    return graph
    
def capture(matrix):
    graph = make_graph(matrix)
    count, attackers, under_attack = 0, set([0]), graph[0]
    captured = lambda ua: len(ua.values()) == 0
    while True:
        count += 1
        attack_on = set(under_attack.items()) # attack on these
        attacked = [] # already attacked in this round
        for a in set(attackers):
            for n,v in attack_on:
                if n in attacked: continue
                attacked.append(n) # dont attack same node again in same loop
                under_attack[n] = v-1 # reduce security by one on each attack
                if v-1 > 0: continue # if security is above 0 continue attack
                attackers.add(n) # node breached, becomes attacker now
                for tba in graph[n]: # new nodes to attack
                    if tba in under_attack or tba in attackers: continue
                    under_attack[tba] = graph[n][tba] # set new nodes connection under attack
                under_attack.pop(n) # remove captured nodes from attack list
        if captured(under_attack): break
    return count

if __name__ == '__main__':
    #These "asserts" using only for self-checking and not necessary for auto-testing
    assert capture([[0,1,0,0,0,0,0,0,0,0],
                    [1,1,1,0,0,0,0,0,0,0],
                    [0,1,2,1,0,0,0,0,0,0],
                    [0,0,1,3,1,0,0,0,0,0],
                    [0,0,0,1,4,1,0,0,0,0],
                    [0,0,0,0,1,5,1,0,0,0],
                    [0,0,0,0,0,1,6,1,0,0],
                    [0,0,0,0,0,0,1,7,1,0],
                    [0,0,0,0,0,0,0,1,8,1],
                    [0,0,0,0,0,0,0,0,1,9]]) == 45, 'relay'
    assert capture([[0, 1, 1],
                    [1, 9, 1],
                    [1, 1, 9]]) == 9, "Small"
    assert capture([[0, 1, 0, 1, 0, 1],
                    [1, 8, 1, 0, 0, 0],
                    [0, 1, 2, 0, 0, 1],
                    [1, 0, 0, 1, 1, 0],
                    [0, 0, 0, 1, 3, 1],
                    [1, 0, 1, 0, 1, 2]]) == 8, "Base example"
    assert capture([[0, 1, 0, 1, 0, 1],
                    [1, 1, 1, 0, 0, 0],
                    [0, 1, 2, 0, 0, 1],
                    [1, 0, 0, 1, 1, 0],
                    [0, 0, 0, 1, 3, 1],
                    [1, 0, 1, 0, 1, 2]]) == 4, "Low security"
