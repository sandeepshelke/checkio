"""You are given number of sticks (N). These sticks are represented in a list
by their length (integers). For this mission, you should calculate the number
of combinations that cannot form a complete triangle (for this mission, the
degenerated case when a+b=c is considered to be a complete triangle).
For example: in the list of lengths [5, 2, 9, 6] the combinations (5, 2, 9)
and (2, 9, 6) would not form a complete triangle. This means that out of all
the possible combinations of numbers in that list, only 2 would not form a
complete triangle.

Input: A list of integers.

Output: An integer.

Example:
    checkio([4, 2, 10]) == 1
    checkio([1, 2, 3]) == 0
    checkio([5, 2, 9, 6]) == 2
"""

def checkio(data):
    dlen = len(data)
    count = 0
    for i in range(dlen-2):
        d0 = data[i]
        for j in range(i+1,dlen):
            d1 = data[j]
            for k in range(j+1,dlen):
                d = [d0,d1,data[k]]
                if not (d[0]+d[1] >= d[2] and d[0]+d[2] >= d[1] and d[1]+d[2] >= d[0]):
                    count += 1

    #replace this for solution
    return count

print(checkio([2,79,47,5,8,57,35,71,27,2]))

#These "asserts" using only for self-checking and not necessary for auto-testing
# if __name__ == '__main__':
#     assert checkio((4, 2, 10)) == 1, "First"
#     assert checkio((1, 2, 3)) == 0, "Second"
#     assert checkio((5, 2, 9, 6)) == 2, "Third"