def build_graph(data, nodes='12345678'):
    graph = {n:[] for n in nodes}
    for s1,s2 in data.split(','):
        graph[s1] += [s2]
        graph[s2] += [s1]

    graph = {n:tuple(v) for n,v in graph.items()}
    return graph

def bfs(graph, nodes=set('12345678'), start='1'):
    path, edges = '1', (set(('1',)),)
    que = [(path, edges)]
    used = []
    while que:
        # edges and path
        path, edges = que.pop(0)
        if set(path) == nodes and path[-1] == start: return path
        if edges in used: continue # ignore if edges used before
        used.append(edges)
        last_node = path[-1] # last visited node
        for n in graph[last_node]: # adjascent nodes to last visited node
            e = set((last_node, n)) # create next possible edge
            if e in edges: continue # ignore the edge if used in present path
            np, ne = path + n, edges + (e,) # next path and corresponding edges
            que.append((np, ne))
    return 'No way!'

def checkio(data):
    #return any route from 1 to 1 over all points
    graph = build_graph(data=data)
    return bfs(graph)

if __name__ == "__main__":
    print(checkio("12,23,34,45,56,67,78,81"))  # "123456781"
    print(checkio("12,28,87,71,13,14,34,35,45,46,63,65"))  # "1365417821"
    print(checkio("12,15,16,23,24,28,83,85,86,87,71,74,56"))  # "12382478561"
    print(checkio("13,14,23,25,34,35,47,56,58,76,68"))  # "132586741"
    print(checkio("12,13,14,15,16,17,18,82,83,84,85,86,87"))