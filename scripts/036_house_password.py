"""
Help Nikola develop a password security check module for Sofia. The password
will be considered strong enough if its length is greater than or equal to 10
symbols, it has at least one number, as well as containing one uppercase letter
and one lowercase letter in it.

Input data: A string that is a password (Unicode for python 2.7).
Output data: The output will be true if the password is safe.
Example:
checkio('A1213pokl')==False
checkio('bAse730onE')==True
checkio('asasasasasasasaas')==False
checkio('QWERTYqwerty')==False
checkio('123456123456')==False
checkio('QwErTy911poqqqq')==True
"""
import re

def checkio(data):
    if len(data) < 10:
        return False
    if not re.search('[A-Z]', data):
        return False
    if not re.search('[a-z]', data):
        return False
    if not re.search('[0-9]', data):
        return False

    return True

if __name__ == '__main__':
    assert checkio('A1213pokl') == False, "1st example"
    assert checkio('bAse730onE4') == True, "2nd example"
    assert checkio('asasasasasasasaas') == False, "3rd example"
    assert checkio('QWERTYqwerty') == False, "4th example"
    assert checkio('123456123456') == False, "5th example"
    assert checkio('QwErTy911poqqqq') == True, "6th example"