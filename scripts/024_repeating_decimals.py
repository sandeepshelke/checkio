"""Input: A list of two integers as a n and d.

Output: A decimal representation of the fraction in bracket format as a string.

Example:
checkio([1, 3]) == "0.(3)", "First"
checkio([5, 3]) == "1.(6)", "Second"
checkio([3, 8]) == "0.375", "Third"
checkio([7, 11]) == "0.(63)", "Fourth"
checkio([29, 12]) == "2.41(6)", "Fifth"
checkio([11, 7]) == "1.(571428)", "Sixth"

How it is used: Mathematical software.
"""
def divide(n,d):
    v = n // d
    n = 10 * (n - v * d)
    return n, v

def ellipsis2bracket(n, d):
    n, v = divide(n, d)
    if n is 0: return str(v)
    result = str(v) + '.'
    states = {}
    while n > 0:
        prev_state = states.get(n, None)
        if prev_state:
            return result[:prev_state] + '(' + result[prev_state:] + ')'
        states[n] = len(result)
        n, v = divide(n, d)
        result += str(v)
    return result

def checkio(fraction):
    n, d = fraction
    return ellipsis2bracket(n, d)

#These "asserts" using only for self-checking and not necessary for auto-testing
if __name__ == '__main__':
    assert checkio([1, 3]) == "0.(3)", "1/3 Classic"
    assert checkio([5, 3]) == "1.(6)", "5/3 The same, but bigger"
    assert checkio([3, 8]) == "0.375", "3/8 without repeating part"
    assert checkio([7, 11]) == "0.(63)", "7/11 prime/prime"
    assert checkio([29, 12]) == "2.41(6)", "29/12 not and repeating part"
    assert checkio([11, 7]) == "1.(571428)", "11/7 six digits"
