"""
Each chip is represented as a list with three natural numbers. You should help
the robots to find the highest score for the given chips and return it as a
number. If you cannot form legal hexagon from the chips, then return 0.

Input: The chips' info. A list of lists. Each list contain three natural
numbers (int).
Output: The highest possible score (int).
"""

def checkio(data):
    highest = sum([max(d) for d in data])
    return 0

if __name__ == '__main__':
    checkio([[1, 4, 20], [3, 1, 5], [50, 2, 3],
             [5, 2, 7], [7, 5, 20], [4, 7, 50]]) == 152
    checkio([[1, 10, 2], [2, 20, 3], [3, 30, 4],
             [4, 40, 5], [5, 50, 6], [6, 60, 1]]) == 210
    checkio([[1, 2, 3], [2, 1, 3], [4, 5, 6],
             [6, 5, 4], [5, 1, 2], [6, 4, 3]]) == 21
    checkio([[5, 9, 5], [9, 6, 9], [6, 7, 6],
             [7, 8, 7], [8, 1, 8], [1, 2, 1]]) == 0