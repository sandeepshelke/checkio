"""
There is a list that contains integers, list of integers or nested lists.
Put all integer values into one list.

Input data: A nested list or simple list.
Output data: A one-dimensional list.
Example:
    checkio([1, 2, 3]) == [1, 2, 3]
    checkio([1, [2, 2, 2], 4]) == [1, 2, 2, 2, 4]
    checkio([[[2]], [4, [5, 6, [6], 6, 6, 6], 7]]) == [2, 4, 5, 6, 6, 6, 6, 6, 7]
"""

def getItems(data):
    nl = []
    for d in data:
        if isinstance(d, list):
            nl += getItems(d)
        else:
            nl += [d]
    return nl

def checkio(data):
    n = getItems(data)
    return n


#These "asserts" using only for self-checking and not necessary for auto-testing
if __name__ == '__main__':
    assert checkio([1, 2, 3]) == [1, 2, 3], 'First example'
    assert checkio([1, [2, 2, 2], 4]) == [1, 2, 2, 2, 4], 'Second example'
    assert checkio([[[2]], [4, [5, 6, [6], 6, 6, 6], 7]]) ==\
        [2, 4, 5, 6, 6, 6, 6, 6, 7], 'Third example'
