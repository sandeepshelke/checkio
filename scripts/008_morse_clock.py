#Generate morse clock as per the input time

def getMorse(num):
    return str(bin(num))[2:].replace('1','-').replace('0','.')
    
def getDigits(num):
    t,u = int(num/10),int(num%10)
    return t,u

def padIt(s, limit):
    padLen = limit - len(s)
    if padLen > 0:
        s = '.'*padLen + s
    return s

def checkio(data):
    hms = [int(i) for i in data.split(':')]
    
    morse = ''
    limit1 = 2
    limit2 = 4
    for i in range(3):
        t,u = getDigits(hms[i])
        mt = getMorse(t)
        mu = getMorse(u)
        if i > 0:
            limit1 = 3
        morse += padIt(mt, limit1) + ' ' + padIt(mu, limit2) + ' : '
    morse = morse[0:-3]
    return morse

#These "asserts" using only for self-checking and not necessary for auto-testing
if __name__ == '__main__':
    assert checkio("10:37:49") == ".- .... : .-- .--- : -.. -..-", "First Test"
    assert checkio("21:34:56") == "-. ...- : .-- .-.. : -.- .--.", "Second Test"
    assert checkio("00:1:02") == ".. .... : ... ...- : ... ..-.", "Third Test"
    assert checkio("23:59:59") == "-. ..-- : -.- -..- : -.- -..-", "Fourth Test"
