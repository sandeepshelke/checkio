#Converts input integer to Roman

roman1 = {0:'',1:'I',2:'II',3:'III',4:'IV',5:'V',6:'VI',7:'VII',8:'VIII',9:'IX'}
roman10 = {0:'',10:'X',20:'XX',30:'XXX',40:'XL',50:'L',60:'LX',70:'LXX',80:'LXXX',90:'XC'}
roman100 = {0:'',100:'C',200:'CC',300:'CCC',400:'CD',500:'D',600:'DC',700:'DCC',800:'DCCC',900:'CM'}
roman1000 = 'M'

def checkio(data):
    n1 = int(data%10) #Units
    n2 = int(data%100) - n1 #Tens
    n3 = int(data%1000) - n1 - n2 #Hundreds
    n4 = int(data%10000/1000) #Thousands

    word = roman1000*n4
    word += roman100[n3]
    word += roman10[n2]
    word += roman1[n1]
    print(n1,n2,n3,n4)
    print(word)
    return word

#These "asserts" using only for self-checking and not necessary for auto-testing
if __name__ == '__main__':
    assert checkio(834) == 'DCCCXXXIV', '834'
    assert checkio(76) == 'LXXVI', '76'
    assert checkio(499) == 'CDXCIX', '499'
    assert checkio(3888) == 'MMMDCCCLXXXVIII', '3888'