"""
The alphabet contains both vowel and consonant letters (Yes, we divide the
letters).
Vowels -- A E I O U Y
Consonants -- B C D F G H J K L M N P Q R S T V W X Z

You are given a block of text with various different words. These words are
separated by whitespaces and punctuation marks. Numbers are not considered
words in this mission (a mix of letters and digits is not a word too). You
should count the number of words (striped words) where the vowels with
consonants are alternating, that is; words that you count cannot have two
consecutive vowels or consonants. The words consisting of a single letter are
not striped -- do not count it. Case is not significant for this mission.

Input: A text. A string (unicode)

Output: A quantity of striped words. An integer.

Example:
checkio("My name is ...") == 3
checkio("Hello world") == 0
checkio("A quantity of striped words.") == 1, "Only of"
checkio("Dog,cat,mouse,bird.Human.") == 3
How it is used: It can be useful for different linguistic researches.
"""
import re

vowels, consonants = "AEIOUY", "BCDFGHJKLMNPQRSTVWXZ"
VOWEL, CONSONANT = 0, 1

def vowel_or_consonant (ch):
    return VOWEL if ch in vowels else CONSONANT

def is_striped(word):
    word = word.upper()
    if not word.isalpha() or len(word) == 1:
        return False
    prev = vowel_or_consonant(word[0])
    for ch in word[1:]:
        vc = vowel_or_consonant(ch)
        if vc == prev:
            return False
        prev = vowel_or_consonant(ch)
    return True

def checkio(data):
    #split using , . ' '
    sentence = re.split('\W+', data)
    count = 0
    words = ''

    for word in sentence:
        if is_striped(word):
            count += 1
            words += word + ', '

    print (data, count, words[:-2])
    return count

if __name__ == '__main__':
    assert checkio("My name is ...") == 3, "All words are striped"
    assert checkio("Hello world") == 0, "No one"
    assert checkio("A quantity of striped words.") == 1, "Only of"
    assert checkio("Dog,cat,mouse,bird.Human.") == 3, "Dog, cat and human"