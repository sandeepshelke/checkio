from decimal import Decimal
def friendly_number(number, base=1000, decimals=0, suffix='',
                    powers=['', 'k', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y']):
    fmt_num = str('{0:.'+str(decimals)+'f}{1}')
    if number == 0: return fmt_num.format(number, suffix)
    number, base = Decimal(number), Decimal(base)
    count, max = 1, len(powers)
    while abs(number) >= base and count < max:
        count += 1
        number /= base

    power = powers[count-1]
    number = round(number, decimals) if decimals else round(number)
    return fmt_num.format(number, power) + suffix

#These "asserts" using only for self-checking and not necessary for auto-testing
if __name__ == '__main__':
    assert friendly_number(102) == '102', '102'
    assert friendly_number(10240) == '10k', '10k'
    assert friendly_number(12341234, decimals=1) == '12.3M', '12.3M'
    assert friendly_number(12461, decimals=1) == '12.5k', '12.5k'
    assert friendly_number(1024000000, base=1024, suffix='iB') == '976MiB', '976MiB'
    assert friendly_number(12000000, decimals=3) == '12.000M', '12.000M'
    assert friendly_number(0, decimals=3, suffix="th") == '0.000th', '0.000th'
