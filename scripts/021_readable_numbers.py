"""
The previous version of robots wasn't able to recognize big numbers. Thus we
should make big numbers more readable and place a dot every three digits from
the end in the numbers longer or equal than 4 digits.
There is no reason to insert dots in ordinal numbers like 1900th.
Input: A string containing numbers (unicode).
Output: Formatted string with dot-separated numbers (str).
"""
def extractNumbers(text):
    text_len = len(text)
    i = 0
    numbers = []
    while i < text_len:
        if text[i].isnumeric():
            num = ''
            while i < text_len and text[i] != ' ':
                num += text[i]
                i+=1
            if num.isnumeric():
                numbers += [num]
        else:
            i+=1
    return numbers

def insertDelim(text, delim, interval):
    if len(text) <= interval:
        return text
    out = ""
    count = 0
    reverse = [i for i in text]
    reverse = reverse[::-1]
    for i in reverse:
        out = i + out
        count += 1
        if count%interval == 0:
            out = delim + out
    if out[0] == delim:
        out = out[1:]
    return out

def checkio(text):
    """
    string with dot separated numbers, which inserted after every third digit from right to left
    """
    if text.isnumeric():
        text = insertDelim(text, ".", 3)
    else:
        numbers = extractNumbers(text)
        for number in numbers:
            num = insertDelim(number, ".", 3)
            text = text.replace(number, num)
    print(text)
    return text

#These "asserts" using only for self-checking and not necessary for auto-testing
if __name__ == '__main__':
    assert checkio('123456') == '123.456', "1st example"
    assert checkio('333') == '333', "2nd example"
    assert checkio('9999999') == '9.999.999', "3rd example"
    assert checkio('123456 567890') == '123.456 567.890', "4th example"
    assert checkio('price is 5799') == 'price is 5.799', "5th example"
    assert checkio('he was born in 1966th') == 'he was born in 1966th', "6th example"
