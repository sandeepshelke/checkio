"""Teach Sofia how to use an ATM. The ATM on their home island can only
dispense $5 bills which means that the machine will not dispense any amount
of money that is not divisible by $5. In addition to that, the bank fee for
each withdrawal is $1. The robots cannot withdraw any amount above the card’s
balance. Also the ATM can not process negative value.

Input: Two arguments: the first one denotes the robot’s account balance, and
the second is a list of the monetary amounts that the robot wants to withdraw.

Output: The account balance after all operations. An integer.

Example:
Withdraw without error
120 - 10 - 1 = 109
109 - 20 - 1 = 88
88 - 30 - 1 = 57
    checkio(120, [10, 20, 30]) == 57
    checkio(120, [200, 10]) == 109
    checkio(120,[3, 10]) == 109
    checkio(120, [200 , 119]) == 120
    checkio(120, [120, 10, 122, 2, 10, 10, 30, 1]) == 56
    checkio(120, [-10]) == 120
"""

TXN_CHARGE = 1

def checkio(balance, withdrawal):
    if balance <= 1:
        return balance

    for trans in withdrawal:
        if trans%5 != 0 or trans <= 0:
            continue
        wamt = trans + TXN_CHARGE
        if wamt <= balance:
            balance -= wamt
        if balance == 0:
            break

    return balance
#Some hints:
#Make sure you loop through the withdrawal amounts
#make sure you have enough money to withdraw,
#otherwise don't (return the same balance)

#These "asserts" using only for self-checking and not necessary for auto-testing
if __name__ == '__main__':
    assert checkio(120, [10, 20, 30]) == 57, 'First example'

    # With one Insufficient Funds, and then withdraw 10 $
    assert checkio(120, [200, 10]) == 109, 'Second example'

    #with one incorrect amount
    assert checkio(120, [3, 10]) == 109, 'Third example'

    assert checkio(120, [200, 119]) == 120, 'Fourth example'

    assert checkio(120, [120, 10, 122, 2, 10, 10, 30, 1]) == 56, "It's mixed all base tests"

    assert checkio(120, [-10]) == 120, 'Negative value'
    
    