def old_solution(text):
    import re
    tl = len(text)
    chars = {}
    for i in range(tl):
        if text[i] == ' ': continue
        if re.search('[.!@#$%^&*?()]',text[i]): continue
 
        cnt = 1
        for j in range(i+1, tl):
            if text[i].lower() == text[j].lower():
                cnt+=1
        if text[i].lower() in str(list(chars.values())).lower(): continue
        try:
            chars[cnt] += [text[i]]
        except KeyError:
            chars[cnt] = [text[i]]
    keys = list(chars.keys())
    print(chars)
    return sorted(chars[keys[-1]], key=str.lower)[0].lower()

def checkio(text):
    t = text.lower()
    alphabets = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.lower()
    letters = [t.count(alpha) for alpha in alphabets]
    return alphabets[letters.index(max(letters))]
    
print(checkio("Gregor then turned to look out the window at the dull weather.Nooooooooooo!!! Why!?!"))
