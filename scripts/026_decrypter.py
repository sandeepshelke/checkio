"""
Help Sofia write a decrypter for the passwords that Nikola will encrypt through
the cipher map. A cipher grille is a 4 × 4 square of paper with four windows cut
out. Placing the grille on a paper sheet of the same size, the encoder writes
down the first four symbols of his password inside the windows (see fig. below).
After that, the encoder turns the grille 90 degrees clockwise. The symbols
written earlier become hidden under the grille and clean paper appears inside the
windows. The encoder then writes down the next four symbols of the password in the
windows and turns the grille 90 degrees again. Then, they write down the following
four symbols and turns the grille once more. Lastly, they write down the final
four symbols of the password. Without the same cipher grille, it is difficult to
discern the password from the resulting square comprised of 16 symbols. Thus, the
encoder can be confident that no hooligan will easily gain access to the locked door.

Write a module that enables the robots to easily recall their passwords through
codes when they return home.

Input: A list of two lists. The first list with four lines contain the Robot's
cipher grille. The next list with four lines contain the square with the ciphered
password. All the symbols in the square are lowercase Latin letters.

Output: The password. A string.

Example:
checkio([[
'X...',
'..X.',
'X..X',
'....'],[
'itdf',
'gdce',
'aton',
'qrdi']
]) == 'icantforgetiddqd'
 
checkio([[
'....',
'X..X',
'.X..',
'...X'],[
'xhwc',
'rsqx',
'xqzz',
'fyzr']
]) == 'rxqrwsfzxqxzhczy'
How it is used: It will help you to learn how to work with 2D arrays.
"""

def get_char_occurance_indices(data, c):
    """Returns a list of all positions of the character in input data,
    empty list if nothing found
    """
    pos = []
    for i in range(len(data)):
        pos += [[i, x] for x, letter in enumerate(data[i]) if letter == c]
    return pos

def rotate_ninety(gril):
    """Rotates the input indices by 90 degrees
    i.e. 
    """
    i, k = 0, 3
    g = [['.']*4, ['.']*4, ['.']*4, ['.']*4]
    for row in gril:
        for j in range(4):
            g[j][k] = row[j]
        i += 1
        k -= 1
    return g

def checkio(data):
    grille, template = data
    gril = []
    for d in grille:
        gril += [[i for i in d]]

    indices = get_char_occurance_indices(gril, 'X')
    gril = rotate_ninety(gril)
    indices += get_char_occurance_indices(gril, 'X')
    gril = rotate_ninety(gril)
    indices += get_char_occurance_indices(gril, 'X')
    gril = rotate_ninety(gril)
    indices += get_char_occurance_indices(gril, 'X')
    password = ''
    for i in indices:
        password += template[i[0]][i[1]]

    return password

#These "asserts" using only for self-checking and not necessary for auto-testing
if __name__ == '__main__':
    assert checkio([
        ['X...',
         '..X.',
         'X..X',
         '....'],
        ['itdf',
         'gdce',
         'aton',
         'qrdi']]) == 'icantforgetiddqd', 'First example'

    assert checkio([
        ['....',
         'X..X',
         '.X..',
         '...X'],
        ['xhwc',
         'rsqx',
         'xqzz',
         'fyzr']]) == 'rxqrwsfzxqxzhczy', 'Second example'
