"""Earlier we learned how to simplify an expression with one variable in the
Polynomial mission. Now you should try your hand at Polynomial division.
Given two polynomial as a list of two strings in next format:
    СN*x*x*...(n times)...*x+CN1*x*x*...(n-1 times)...*x+...+C1*x+C0
    where
    CN, CN1 ...C1, C0 - constants, integers.
    if C = 0, then don't show this term (ex. 2*x*x+3).
    if C = 1, then don't show C. (ex. x*x+2*x), except C0.
    if C = -1, then don't show C, only "-". (ex. x*x-x), except C0.
    
You should to divide the first polynomial by the second and return the result
with remainder as a list of two strings. A remainder of 0 should be returned as
an empty string.
Input: A list of two strings. A dividend, a divisor.
Output: A list of two strings. A result of division, a remainder.
"""
 
def degree(poly):
    while poly and poly[-1] == 0:
        poly.pop()   # normalize
    return len(poly)-1
 
def poly_div(N, D):
    dD = degree(D)
    dN = degree(N)
    q = [0] * dN
    while dN >= dD:
        d = [0]*(dN - dD) + D
        mult = q[dN - dD] = N[-1] / float(d[-1])
        d = [coeff*mult for coeff in d]
        N = [(coeffN - coeffd) for coeffN, coeffd in zip(N, d)]
        dN = degree(N)
    r = N
    return q, r

def getPolyAsIndex(eq):
    poly = []
    eq_len = len(eq)
    i = 0
    prev_sign = "+"
    while i < eq_len:
        deg = 0
        coef = ""
        while i < eq_len and eq[i] != '+' and eq[i] != '-':
            if eq[i].isnumeric():
                coef += eq[i] 
            elif eq[i] == 'x':
                deg += 1
            i+=1

        if coef == "":
            coef = "1"
        
        poly += [prev_sign, coef, deg]
        if i < eq_len:
            prev_sign = eq[i]
            i+=1

    new_poly = [0]*(poly[2]+1)
    i = 0
    while i < len(poly):
        if poly[i] == '-':
            new_poly[poly[i+2]] = -int(poly[i+1])
        else:
            new_poly[poly[i+2]] = int(poly[i+1])
        i+=3

    return new_poly

def getPolyFromIndex(poly):
    eq = ""
    if len(poly) == 0:
        return eq
    if poly[0] != 0:
        eq = str(poly[0])
    if len(poly) > 1 and poly[0] > 0:
        eq = "+" + eq

    for i in range(1,len(poly)):
        if poly[i] == 0:
            continue

        eq = "*x"*(i-1) + eq

        if poly[i] == 1:
            eq = "+x" + eq
        elif poly[i] == -1:
            eq = "-x" + eq
        elif poly[i] > 1:
            eq = "+" + str(poly[i]) + "*x" + eq
        elif poly[i] < -1:
            eq = str(poly[i]) + "*x" + eq
             
    if len(eq) and eq[0] == '+':
        eq = eq[1:]   
    return eq

def checkio(data):
    dividend, divider = data
    dividend_poly = getPolyAsIndex(dividend)
    divider_poly = getPolyAsIndex(divider)

    q, r = poly_div(dividend_poly, divider_poly)
    q = [int(i) for i in q]
    r = [int(i) for i in r]
    q, r = getPolyFromIndex(q), getPolyFromIndex(r)

    #replace this for solution
    return [q, r]

print(checkio(["x*x+1","x*x*x"]))

#These "asserts" using only for self-checking and not necessary for auto-testing
# if __name__ == '__main__':
#     assert checkio(['x*x*x-12*x*x-42', 'x-3']) == ['x*x-9*x-27', '-123'], "1st"
#     assert checkio(['x*x-1', 'x+1']) == ['x-1', ''], "2nd"
#     assert checkio(['x*x*x+6*x*x+12*x+8', 'x+2']) == ['x*x+4*x+4', ''], "3rd"
#     assert checkio(['x*x*x*x*x+5*x+1', 'x*x']) == ['x*x*x', '5*x+1'], "4th"
