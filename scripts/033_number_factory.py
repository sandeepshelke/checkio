"""
Given an integer N. You should find the smallest positive number of X, such
that the product of its digits is equal to the number of N. If number X does
not exist, then return 0.

Hints: Remember about prime numbers (or numbers divided at non one digit prime
number) and be careful with eternal loops.

Input: An Integer.

Output: An Integer.

Example:
checkio(20) == 45
checkio(21) == 37
checkio(17) == 0
checkio(33) == 0
checkio(5) == 5
How it is used: This task will learn you how to work with numbers.
"""
from math import floor

def is_prime(n):
    """Determines whether number is prime number
    Args:
        n imput integer value

    Returns:
        True if number is prime, False otherwise
    """
    if n == 2:
        return True
    if not n & 1:
        return False
    if n < 2:
        return False

    for x in range(3, int(n**0.5)+1, 2):
        if n % x == 0:
            return False
    return True

def factors(n):
    result = []
    for i in range(2,n+1): # test all integers between 2 and n
        s = 0;
        while n/i == floor(n/float(i)): # is n/i an integer?
            n = n/float(i)
            s += 1
        if s > 0:
            for _ in range(s):
                result.append(i) # i is a pf s times
        if n == 1:
            break

    m = []
    for i in result:
        s = str(i)
        if len(s) > 1:
            m += [int(x) for x in s]
        else:
            m+=[i]

    return m

def product(data):
    p = 1
    for i in data:
        p*=i
    return p

def concate_num(data):
    s = ''
    for x in data:
        s += str(x)
    return int(s)
    

def checkio(data):
    if 0 < data < 10:
        return data
    if is_prime(data):
        return 0
    pf = sorted(factors(data))
    if product(pf) != data:
        return 0
    npf = []
    i, pf_len = 0, len(pf)
    while i < pf_len:
        npf += [pf[i]]
        n = pf[i]
        i += 1
        if i >= pf_len:
            break
        if (n == 2 and pf[i] == 3) or (n == 3 and pf[i] <= 3):
            #for combination 2,3 or 3,2/3
            npf[-1] *= pf[i]
            i+=1
        elif n == pf[i] == 2:
            #for combination 2,2
            npf[-1] *= 2
            i+=1
            if pf[i] == 2:
                #for combination 2,2,2
                npf[-1] *= 2
                i += 1
    npf = sorted(npf)
    return concate_num(npf)

#These "asserts" using only for self-checking and not necessary for auto-testing
if __name__ == '__main__':
    assert checkio(20) == 45, "1st example"
    assert checkio(21) == 37, "2nd example"
    assert checkio(17) == 0, "3rd example"
    assert checkio(33) == 0, "4th example"
    assert checkio(5) == 5, "5th example"
    print(checkio(560))
    print(checkio(1680))
    print(checkio(1536))
