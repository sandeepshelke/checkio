#Xs and Os
#A tic-tac-toe match result refree

def isWinner(data):
    if data[0] == data[1] == data[2]:
        return data[0]
    return 'D'

def checkio(game_result):
    gr = game_result
    #row and column size
    size = len(gr)

    for i in range(size):
        if isWinner(gr[i]) != 'D':
            return gr[i][0]
        comb = ''
        for j in range(size):
            comb += gr[j][i]
        if isWinner(comb) != 'D':
            return comb[0]
        
    i, j, k = 0, 0, size-1
    d1, d2 = '', ''
    while i < size and j < size and k > -1:
        d1 += gr[i][j]
        d2 += gr[i][k]
        i, j = i+1, j+1
        k -= 1

    if isWinner(d1) != 'D':
        return d1[0]
    if isWinner(d2) != 'D':
        return d2[0]
    return 'D'
    
#These "asserts" using only for self-checking and not necessary for auto-testing
if __name__ == '__main__':
    assert checkio([
        "X.O",
        "XX.",
        "XOO"]) == "X", "Xs wins"
    assert checkio([
        "OO.",
        "XOX",
        "XOX"]) == "O", "Os wins"
    assert checkio([
        "OOX",
        "XXO",
        "OXX"]) == "D", "Draw"

   
def checkio_old(game_result):
    r1 = game_result[0]
    r2 = game_result[1]
    r3 = game_result[2]
    c1 = r1[0]+r2[0]+r3[0]
    c2 = r1[1]+r2[1]+r3[1]
    c3 = r1[2]+r2[2]+r3[2]
    d1 = r1[0]+r2[1]+r3[2]
    d2 = r1[2]+r2[1]+r3[0]

    if isWinner(r1) != 'D':
        return r1[0]

    if isWinner(r2) != 'D':
        return r2[0]

    if isWinner(r3) != 'D':
        return r3[0]
    
    if isWinner(c1) != 'D':
        return c1[0]

    if isWinner(c2) != 'D':
        return c2[0]

    if isWinner(c3) != 'D':
        return c3[0]
    
    if isWinner(d1) != 'D':
        return d1[0]

    if isWinner(d2) != 'D':
        return d2[0]

    return "D"
