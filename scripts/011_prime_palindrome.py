"""An integer is said to be a palindrome if it is equal to its reverse in a
string form. For example, 79197 and 324423 are palindromes. In this task you
will be given an integer N. You must find the smallest integer M >= N such
that M is a prime number and also a palindrome. 

Input: N. An integer.

Output: A prime palindrome. An integer.

Example:
    checkio(31) == 101
    checkio(130) == 131
    checkio(131) == 131
"""
def is_prime(n):
    """Determines whether number is prime number
    Args:
        n imput integer value

    Returns:
        True if number is prime, False otherwise
    """
    if n < 2:
        return False
    elif n == 2:
        return True
    elif not n & 1:
        return False
    elif n % 2 == 0:
        return False

    for x in range(3, int(n**0.5)+1, 2):
        if n % x == 0:
            return False
    return True

def is_palindrome(pStr):
    return True if pStr == pStr[::-1] else False

def checkio(number):
    palprime = number
    if is_palindrome(str(palprime)) and is_prime(palprime):
        return palprime

    palprime += 1
    while palprime >= number:
        if is_prime(palprime) and is_palindrome(str(palprime)):
            break
        palprime+=1
    
    return palprime

#These "asserts" using only for self-checking and not necessary for auto-testing
if __name__ == '__main__':
    assert checkio(31) == 101, 'First example'
    assert checkio(130) == 131, 'Second example'
    assert checkio(131) == 131, 'Third example'