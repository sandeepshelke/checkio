R, L, D, U, B, W, S, E = 'RLDUBWSE'
MOVES = {R:(0,1), L:(0,-1), D:(1,0), U:(-1,0)}

def get_low_cost_node(opennodes, fscore):
    node, cost = '', 100
    for n in opennodes:
        if n in fscore and fscore[n] < cost:
            node, cost = n, fscore[n]
    return node

def distance(current, neighbor):
    c1,c2 = current
    n1,n2 = neighbor
    c1,c2,n1,n2 = int(c1),int(c2),int(n1),int(n2)
    import math
    return math.sqrt((n1-c1)**2+(n2-c2)**2)

def astar(graph, start, end):
    opennodes, closednodes, camefrom = [start], set(), {}
    gscore, fscore = {start: 0}, {start: 0} #heuristic
    while opennodes:
        current = get_low_cost_node(opennodes, fscore)
        if current == end: break
        opennodes.remove(current)
        closednodes.add(current)
        _, edges = graph[current]
        for n,d in edges:
            if n in closednodes: continue
            if n not in opennodes: opennodes.append(n)
            camefrom[n] = (current, d)
            score = gscore[current] + distance(current, n)
            gscore[n], fscore[n] = score, score + 1
    return camefrom

def make_graph(data):
    key = lambda i,j: str(i)+str(j)
    graph, start, end = {}, '', ''
    sizei, sizej = len(data), len(data[0])
    valid = lambda i,j: 0 <= i < sizei and 0 <= j < sizej and data[i][j] != W
    for i in range(sizei):
        for j in range(sizej):
            cs = data[i][j] # cell state
            if cs == W: continue # ignore wall
            if cs == S: start = key(i,j)
            elif cs == E: end = key(i,j)
            edges = [(key(i+mi,j+mj),d) for d, (mi, mj) in MOVES.items() if valid(i+mi,j+mj)]
            if edges: graph[key(i,j)] = (cs, edges,)
    return graph, start, end

def get_path(graph, start, end):
    camefrom = astar(graph, start, end)
    cur, found = end, False
    dirs, route = [], [cur]
    for _ in camefrom:
        cur, d = camefrom[cur]
        route.append(cur)
        dirs.append(d)
        found = cur == start
        if found: break
    
    if not found: return None
    dirs, route = dirs[::-1], route[::-1]
    return {'dirs': dirs, 'route': route}

def all_paths(starts, ends, graph):
    connectors = {}
    for s in starts:
        connectors[s] = {e: get_path(graph, s, e) for e in ends if s != e}
    return connectors

def cost_of_path(p):
    if B not in p: return len(p)*2
    p = p.split(B)
    return len(p[0])*2 + len(p[1]) + len(p[2])*2 + 2

def formpath(dirs, route, graph):
    # include if you are beggining and ending at box
    hasbox = [i-1 for i,n in enumerate(route) if graph[n][0] == B and 0 < i < len(route)]
    if len(hasbox) < 2: return ''.join(dirs)
    b1,b2=hasbox[0],hasbox[-1] #use first and last ignore middle
    if abs(b1-b2) == 1: return ''.join(dirs) # ignore adjescent boxes
    d = list(dirs)
    d[b1]+=B
    d[b2]+=B
    return ''.join(d)

def get_fastest_path(graph, start, end):
    boxes = [k for k,(c,_) in graph.items() if c==B]
    dp = all_paths([start], [end], graph)
    if start not in dp or end not in dp[start] or not dp[start][end]: return 'No way'
    # directpath exists, now find fastest path

    stb = all_paths([start], boxes, graph)[start] # start to boxes
    bte = all_paths(boxes, [end], graph) # boxes to end
    bte = {k: v[end] for k,v in bte.items()}
    btb = all_paths(stb.keys(), bte.keys(), graph) # boxes to boxes

    d,r = dp[start][end]['dirs'], dp[start][end]['route']
    fastp = formpath(d,r,graph)
    fastc = cost_of_path(fastp)
    paths = {fastp: fastc}
    for k, sb in stb.items():
        for b,bb in btb[k].items():
            d = sb['dirs'] + bb['dirs'] + bte[b]['dirs']
            r = sb['route'] + bb['route'][1:-1] + bte[b]['route']
            fp = formpath(d,r,graph)
            fc = cost_of_path(fp)
            paths[fp] = fc
            if fastc > fc: fastp,fastc=fp,fc
    return fastp

def checkio(field_map):
    graph, start, end = make_graph(field_map)
    return get_fastest_path(graph, start, end)

if __name__ == '__main__':
    #This part is using only for self-checking and not necessary for auto-testing
    ACTIONS = {
        "L": (0, -1),
        "R": (0, 1),
        "U": (-1, 0),
        "D": (1, 0),
        "B": (0, 0)
    }

    def check_solution(func, max_time, field):
        max_row, max_col = len(field), len(field[0])
        s_row, s_col = 0, 0
        total_time = 0
        hold_box = True
        route = func(field[:])
        for step in route:
            if step not in ACTIONS:
                print("Unknown action {0}".format(step))
                return False
            if step == "B":
                if hold_box:
                    if field[s_row][s_col] == "B":
                        hold_box = False
                        total_time += 1
                        continue
                    else:
                        print("Stephan broke the cargo")
                        return False
                else:
                    if field[s_row][s_col] == "B":
                        hold_box = True
                    total_time += 1
                    continue
            n_row, n_col = s_row + ACTIONS[step][0], s_col + ACTIONS[step][1],
            total_time += 2 if hold_box else 1
            if 0 > n_row or n_row >= max_row or 0 > n_col or n_row >= max_col:
                print("We've lost Stephan.")
                return False
            if field[n_row][n_col] == "W":
                print("Stephan fell in water.")
                return False
            s_row, s_col = n_row, n_col
            if field[s_row][s_col] == "E" and hold_box:
                if total_time <= max_time:
                    return True
                else:
                    print("You can deliver the cargo faster.")
                    return False
        print("The cargo is not delivered")
        return False

    assert check_solution(checkio, 16, ["S...BB..E"]), 'wierd'
    assert check_solution(checkio, 35, ["SW.B.W.",".W.W.W.",".W.W.W.",".W.W.WB",".W.W.W.",".B.W..E"]), 'weird2'
    assert check_solution(checkio, 18, ["SBW...",".WWB..","..WW..","......","...WW.","..BWBE"]), '3rd example'
    assert check_solution(checkio, 12, ["S...", "....", "B.WB", "..WE"]), "1st Example"
    assert check_solution(checkio, 11, ["S...", "....", "B..B", "..WE"]), "2nd example"
