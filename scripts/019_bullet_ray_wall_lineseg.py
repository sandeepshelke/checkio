""" Bullet and wall
The wall is represented by two coordinates W1 (xw1, yw1) and W2 (xw2, yw2)
on a coordinate plane. The bullet flies from point "A" (xa, ya), and the
direction of its flight is given by the second point "B" (xb, yb). Determine
whether the bullet hits the wall or not if gravity is not a factor. A != B.
Input: A list with coordinates in next order W1, W2, A, B. Each coordinate
is a list of x and y coordinates (int).
Output: Whether the bullet hits the wall or not. A boolean.
"""
import math

class Point:
    def __init__(self, data):
        self.x = data[0]
        self.y = data[1]

def inLineHit(A,B,C,D):
    #     /   or  /   or  \   or  \
    #    /       /         \       \
    # /            /     \            \
    #/            /       \            \
    if A.y == C.y or A.y == D.y or B.y == C.y or B.y == D.y:
        return False
    #|   or   |   or --    or   --
    #|        |        --     --
    # |      |         
    # |      |
    if A.x == C.x or A.x == D.x or B.x == C.x or B.x == D.x:
        return False

    #   /   or   \    or  -- --
    #  /          \   
    # /            \  
    #/              \ 
    if A.x > D.x > C.x or A.x < D.x < C.x:
        return True
    # |   
    # |   
    # |            
    # |
    if A.y > D.y > C.y or A.y < D.y < C.y:
        return True
    return False

def calcDenom(A,B,C,D):
    return (A.x-B.x)*(C.y-D.y) - (A.y-B.y)*(C.x-D.x)

def intersection(A,B,C,D, denom):
    x = (A.x*B.y-A.y*B.x)*(C.x-D.x) - (A.x-B.x)*(C.x*D.y-C.y*D.x)
    x /= denom
    y = (A.x*B.y-A.y*B.x)*(C.y-D.y) - (A.y-B.y)*(C.x*D.y-C.y*D.x)
    y /= denom
    return x,y

def isCommonPoint(A,B,C,D):
    if (A.x == C.x and A.y == C.y) or (A.x == D.x and A.y == D.y):
        return True
    if (B.x == C.x and B.y == C.y) or (B.x == D.x and B.y == D.y):
        return True
    return False

def isBetween(a, b, c):
    crossproduct = (c.y - a.y) * (b.x - a.x) - (c.x - a.x) * (b.y - a.y)
    if abs(crossproduct) > 0 : return False   # (or != 0 if using integers)

    dotproduct = (c.x - a.x) * (b.x - a.x) + (c.y - a.y)*(b.y - a.y)
    if dotproduct < 0 : return False

    squaredlengthba = (b.x - a.x)*(b.x - a.x) + (b.y - a.y)*(b.y - a.y)
    if dotproduct > squaredlengthba: return False

    return True

def slope(A, B):
    """Returns slope of line
    Throws: ZeroDivisionError
    """
    return (B.y - A.y)/(B.x - A.x)

def getAngle(A,B):
    a = 0
    if A.x == B.x:
        a = 90
        if A.y > B.y:
            a = 270
    elif A.y == B.y:
        if B.x < A.x:
            a = 180
    else:
        a = math.degrees(math.atan((A.y-B.y)/(A.x-B.x)))
    if a < 0:
        a += 180
    return a

def checkio(data):
    A = Point(data[0])
    B = Point(data[1])
    C = Point(data[2])
    D = Point(data[3])
    if isCommonPoint(A,B,C,D):
        return True
    denom = calcDenom(A,B,C,D)
    if denom == 0:
        return inLineHit(A,B,C,D)
    x, y = intersection(A,B,C,D, denom)
    maxy = max(A.y,B.y)
    miny = min(A.y,B.y)
#     maxx = max(A.x,B.x)
#     minx = min(A.x,B.x)
    P = Point([x,y])

    if not (A.x <= P.x <= B.x or A.x >= P.x >= B.x):
        return False

    if maxy >= y >= miny:
        if P.x > D.x > C.x or P.x < D.x < C.x or C.x > P.x > D.x or C.x < P.x < D.x:
            return True

    if C.x == D.x == P.x:
        if C.y > D.y > P.y or C.y < D.y < P.y or C.y > P.y > D.y or C.y < P.y < D.y:
            return True

    if C.y == D.y == P.y:
        if C.x > D.x > P.x or C.x < D.x < P.x or C.x > P.x > D.x or C.x < P.x < D.x:
            return True

    return False

if __name__ == '__main__':
    print(checkio([[8,1],[9,7],[6,6],[6,3]]), False)
    print(checkio([[4,9],[5,9],[7,4],[2,6]]), False)
    print(checkio([[0,1],[1,2],[2,0],[0,2]]), True)
    print(checkio([[0, 0], [0, 2], [5, 1], [3, 1]]), True, "1st example")
    print(checkio([[0, 0], [0, 2], [3, 1], [5, 1]]), False, "2nd example")
    print(checkio([[0, 0], [2, 2], [6, 0], [3, 1]]), True, "3rd example")
    print(checkio([[6, 0], [5, 5], [4, 0], [5, 6]]), False, "4th example")
    print(checkio([[0, 0], [1, 1], [3, 3], [2, 2]]), True, "5th example")
    print(checkio([[0, 0], [1, 1], [3, 2], [2, 1]]), False, "6th example")
         
    print('\nvertical line')
    print(checkio([[7, 0], [7, 2], [5, 1], [3, 1]]), False, "1 example")
    print(checkio([[7, 0], [7, 2], [3, 1], [5, 1]]), True, "2 example")
    print(checkio([[7, 0], [7, 2], [8, 1], [9, 1]]), False, "3 example")
    print(checkio([[7, 0], [7, 2], [9, 1], [8, 1]]), True, "4 example")
           
    print('\nhorizontal line\n')
    print(checkio([[0, 5], [5, 5], [4, 1], [4, 3]]), True, "1 example")
    print(checkio([[0, 5], [5, 5], [4, 3], [4, 1]]), False, "2 example")
    print(checkio([[0, 5], [5, 5], [4, 7], [4, 8]]), False, "3 example")
    print(checkio([[0, 5], [5, 5], [4, 8], [4, 7]]), True, "4 example")
         
    print('\nincreasing line\n')
    print(checkio([[0, 0], [7, 7], [4, 1], [4, 3]]), True, "1 example")
    print(checkio([[0, 0], [7, 7], [4, 3], [4, 1]]), False, "2 example")
    print(checkio([[0, 0], [7, 7], [4, 7], [4, 8]]), False, "3 example")
    print(checkio([[0, 0], [7, 7], [4, 8], [4, 7]]), True, "4 example")
    print('\ndecreasing line\n')
    print(checkio([[0, 7], [8, 0], [4, 1], [4, 3]]), True, "1 example")
    print(checkio([[0, 7], [8, 0], [4, 3], [4, 1]]), False, "2 example")
    print(checkio([[0, 7], [8, 0], [4, 7], [4, 8]]), False, "3 example")
    print(checkio([[0, 7], [8, 0], [4, 8], [4, 7]]), True, "4 example")
         
    print('\ninline')
    print(checkio([[3, 3], [4, 4], [5, 5], [6, 6]]), False, "1 example")
    print(checkio([[3, 3], [4, 4], [1, 1], [2, 2]]), True, "2 example")
    print(checkio([[3, 3], [4, 4], [2, 2], [1, 1]]), False, "3 example")
