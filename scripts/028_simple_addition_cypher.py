"""
The robots are using a very simple symmetric cipher with a special key for
their correspondence with the Home Island. The text contains only lowercase
letters and whitespaces. Each character is assigned a number which corresponds
to its order in the alphabet (a whitespace is represented by 0.) The secret key
is a word that has been translated into a sequence of numbers. That key is then
applied to the block of text they wish to send.

For encryption, the value of the first letter in the block is added to the first
letter in the secret key and modulo 27 (which finds the remainder of division by
27). This is repeated for all of the letters in the block.

For decryption, the value of the first letter in the block is reduced by number
of the first letter in the secret key. If it is a negative number, add 27 to it.
The secret key is used throughout the text blocks cyclically. After decryption,
the sequence of numbers is translated into text.

For example:


Sofia does not have the current key, and has come across a new encrypted message. 
She still has the old encrypted and decrypted messages though, so using this
information you must try to decrypt the new message.

Input: A list of three strings: old encrypted text, old decrypted text, new
encrypted text.

Output: The string of the new decrypted text.

checkio(['lmljemxbwrhhfyd stlmhrxyvwhk',
          'this text contain the secret',
          'tsgryaaxckjqledcxhshreyuasckmysxhuwyl'
    ]) == 'and this text also contain the secret'
How it is used: Decryption software.
"""
num2alpha = [' ','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o',\
             'p','q','r','s','t','u','v','w','x','y','z']

alpha2num = {' ':0,'a':1,'b':2,'c':3,'d':4,'e':5,'f':6,'g':7,'h':8,'i':9,'j':10,\
             'k':11,'l':12,'m':13,'n':14,'o':15,'p':16,'q':17,'r':18,'s':19,\
             't':20,'u':21,'v':22,'w':23,'x':24,'y':25,'z':26}

def getNumSeries(data):
    global alpha2Num
    return [alpha2num[c] for c in data]

def getAlphaSeries(data):
    global num2alpha
    s = ''
    for a in data:
        s += num2alpha[a]
    return s

def getActualKey(key):
    """Extracts the key by eliminating repeatation of key
    """
    #How to solve
    #[19, 5, 3, 18, 5, 20, 11, 5, 25, 19, 5, 3]
    #This one is real tricky part now and will have to be solved with
    #assumption that we found the repetition
    n = []
    extracted = False
    for i in range(1,len(key)):
        ni = 0
        if key[i] == key[ni]:
            n += [key[ni]]
            ni += 1
            for j in range(i+1, len(key)):
                if key[j] == key[ni]:
                    n += [key[j]]
                else:
                    n = []
                    break
                if key[:i] == n:
                    extracted = True
                    break
                ni += 1
            if extracted:
                break
    if not extracted:
        n = key
        
    return n

def checkio(data):
    old_encrypted, old_decrypted, new_encrypted = data
    old_e_num = getNumSeries(old_encrypted)
    old_d_num = getNumSeries(old_decrypted)
    new_e_num = getNumSeries(new_encrypted)
    key = [x+27 if x < 0 else x for x in [a-b for a,b in zip(old_e_num,old_d_num)]]
    if len(new_e_num)-len(key) > 0:
        key = getActualKey(key)
        key = key*(1+(len(new_e_num)//len(key))+1)
    key = key[:len(new_e_num)]
    new_d_num = [x+27 if x < 0 else x for x in [a-b for a,b in zip(new_e_num, key)]]
    new_decrypted = getAlphaSeries(new_d_num)
    return new_decrypted

#These "asserts" using only for self-checking and not necessary for auto-testing
if __name__ == '__main__':
    assert checkio(
        [
            'lmljemxbwrhhfyd stlmhrxyvwhk',
            'this text contain the secret',
            'tsgryaaxckjqledcxhshreyuasckmysxhuwyl'
        ]) == 'and this text also contain the secret', 'Secret'
    assert checkio(
        [
            'pjxxchnedonncdhhrqdgilq',
            'hello its first message',
            'pjxxchnedo jo bleyqg fsq'
        ]) == 'hello its second message', "Hello"
    assert checkio(
        [
            'dxb dxb dxb dxb',
            'bla bla bla bla',
            'tqblbefxv'
        ]) == 'real text', "Bla Bla Bla"
    assert checkio(
        [
            'dtqyefpxqtlh',
            'long message',
            'kmriy'
        ]) == 'short', "Short"
    assert checkio(
        [
            'vjwclkjfijm',
            'keyofsecret',
            'lpcmuyxhuwyd'
        ]) == 'akeyofsecret', "Almost"